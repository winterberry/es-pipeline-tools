#!/bin/bash

# Install jq if it is not already installed

# Check if jq is already installed
if ! command -v jq &> /dev/null; then
    # Detect the architecture
    arch=$(uname -m)

    # Set the download URL based on the architecture
    if [ "$arch" = "x86_64" ]; then
        url="https://github.com/jqlang/jq/releases/download/jq-1.7/jq-linux64-amd64"
    elif [ "$arch" = "aarch64" ] || [ "$arch" = "arm64" ]; then
        url="https://github.com/jqlang/jq/releases/download/jq-1.7/jq-linux-arm64"
    else
        echo "Unsupported architecture: $arch"
        exit 1
    fi

    # Download jq
    curl_response=$(curl -L -o /usr/local/bin/jq "$url" -w "%{http_code}")
    if [ "$curl_response" != "200" ]; then
        echo "Error downloading jq: HTTP response code $curl_response"
        exit 1
    fi

    # Make jq executable
    chmod +x /usr/local/bin/jq

    echo "jq installed successfully"
else
    echo "jq is already installed"
fi
