#!/bin/bash

# Check if required environment variables are set
ensure_environment() {
    required_vars=(ELASTICSEARCH_USERNAME ELASTICSEARCH_PASSWORD ELASTICSEARCH_ADDR ELASTICSEARCH_PORT PIPELINE_NAME)
    for var in "${required_vars[@]}"; do
        if [ -z "${!var}" ]; then
            echo "Error: $var environment variable is not set"
            exit 1
        fi
    done

    # Check if jq is installed and working
    if ! command -v jq &> /dev/null; then
        echo "Error: jq is not installed or not working properly"
        exit 1
    fi
}

# make GET requests to Elasticsearch API
elasticsearch_api_get() {
    curl -s -k -u "${ELASTICSEARCH_USERNAME}:${ELASTICSEARCH_PASSWORD}" "$@"
}

# make PUT requests to Elasticsearch API
# expects a --datafile argument that contains the link to the JSON data that is being sent.
elasticsearch_api_put() {
    curl -s -k -X PUT -u "${ELASTICSEARCH_USERNAME}:${ELASTICSEARCH_PASSWORD}" -H "Content-Type: application/json" --data "@$2" "$3"
}

# make POST requests to Elasticsearch API
# expects a --datafile argument that contains the link to the JSON data that is being sent.
elasticsearch_api_post() {
    curl -s -k -X POST -u "${ELASTICSEARCH_USERNAME}:${ELASTICSEARCH_PASSWORD}" -H "Content-Type: application/json" --data "@$2" "$3"
}

export elasticsearch_url="https://${ELASTICSEARCH_ADDR}:${ELASTICSEARCH_PORT}"
export health_endpoint="${elasticsearch_url}/_cluster/health?wait_for_status=yellow&timeout=60s"
export pipeline_endpoint="${elasticsearch_url}/_ingest/pipeline/${PIPELINE_NAME}"