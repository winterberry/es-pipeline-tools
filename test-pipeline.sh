#!/bin/bash

# We test whether the pipeline is working by simulating an ingest
# and checking the results.

script_dir="$(dirname "${BASH_SOURCE[0]}")"

source "${script_dir}/utils.sh"
source "${script_dir}/install-jq.sh"

ensure_environment

simulate_endpoint="${pipeline_endpoint}/_simulate"

# Test the pipeline by simulating an ingest
response=$(elasticsearch_api_post --datafile "${script_dir}/testdata.json" "${simulate_endpoint}")
echo "Received response from elasticsearch API: ${response}"

actual_result=$(echo "${response}" | jq '[.docs[].doc._source]')

expected_result=$(cat "${script_dir}/expected-results.json")

if [ "${actual_result}" != "${expected_result}" ]; then
    echo "Pipeline test failed. Expected:"
    echo "${expected_result}"
    echo "Actual:"
    echo "${actual_result}"
    exit 1
fi

echo "Pipeline test passed."
