#!/bin/bash
# shellcheck source=utils.sh

# Install the elasticsearch pipeline that processes the IRC messages.

script_dir="$(dirname "${BASH_SOURCE[0]}")"

source "${script_dir}/utils.sh"
source "${script_dir}/install-jq.sh"

ensure_environment

# Wait for Elasticsearch to be ready
while true; do
    response=$(elasticsearch_api_get "${health_endpoint}?wait_for_status=yellow&timeout=60s")
    echo "Received response from health endpoint: ${response}"
    status=$(echo "${response}" | jq -r '.status')
    if [ "${status}" = "green" ] || [ "${status}" = "yellow" ]; then
        break
    fi
    echo "Waiting for Elasticsearch..."
    sleep 5
done

echo "Elasticsearch is up. Creating pipeline..."

# Create the pipeline
response=$(elasticsearch_api_put --datafile ${script_dir}/irc-message-parser.json "${pipeline_endpoint}")
acknowledged=$(echo "${response}" | jq -r '.acknowledged')

if [ "${acknowledged}" != "true" ]; then
    echo "Failed to create pipeline. Acknowledged: ${acknowledged}"
    echo "Response content: ${response}"
    exit 1
fi

echo "Pipeline successfully created."

# ensure that the pipeline is actually working as expected
source "${script_dir}/test-pipeline.sh"